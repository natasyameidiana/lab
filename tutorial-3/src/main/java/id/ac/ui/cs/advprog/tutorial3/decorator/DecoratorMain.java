package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void main(String[] args) {
        Food crustySandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();

        crustySandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(crustySandwich);
        System.out.println(crustySandwich.getDescription());

        crustySandwich = FillingDecorator.CHEESE.addFillingToBread(crustySandwich);
        System.out.println(crustySandwich.getDescription());
    }
}
