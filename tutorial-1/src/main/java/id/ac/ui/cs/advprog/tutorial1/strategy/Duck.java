package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    protected FlyBehavior flyBehavior;
    protected QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public void setFlyBehavior(FlyBehavior flyB) {
        flyBehavior = flyB;
    }

    public void setQuackBehavior(QuackBehavior quackB) {
        quackBehavior = quackB;
    }

    public abstract void display();

    public void swim(){}

}
