package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.jupiter.api.Assertions.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.TastyClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class VeggiePizzaTest {
    private Pizza pizza;
    private PizzaIngredientFactory factory;

    @Before
    public void setUp() {
        factory = new PizzaIngredientFactory() {
            @Override
            public Dough createDough() {
                return new ThickCrustDough();
            }

            @Override
            public Sauce createSauce() {
                return new MarinaraSauce();
            }

            @Override
            public Cheese createCheese() {
                return new MozzarellaCheese();
            }

            @Override
            public Veggies[] createVeggies() {
                return new Veggies[]{new Spinach()};
            }

            @Override
            public Clams createClam() {
                return new TastyClams();
            }
        };

        pizza = new VeggiePizza(factory);
        pizza.setName("New Variant of Veggie Pizza");
    }

    @Test
    public void testPrepareMethod() {
        pizza.prepare();
        assertTrue(pizza.dough instanceof ThickCrustDough);
        assertTrue(pizza.sauce instanceof MarinaraSauce);
        assertTrue(pizza.cheese instanceof MozzarellaCheese);
        assertTrue(pizza.veggies[0] instanceof Spinach);
    }
}

