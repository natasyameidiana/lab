package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    private static Singleton instance = null;

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    private Singleton() {

    }

    public static Singleton getInstance() {
        // TODO Implement me!
        if (instance == null) {
            instance = new Singleton();
        }

        return instance;

    }
}
